import React from "react";

export default function Footer() {
  return (
    <footer className="footer bg-dark mt-1">
      <p>&copy; {new Date().getFullYear()} Fitness Quest</p>
    </footer>
  );
}
