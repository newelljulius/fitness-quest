import React, { useEffect, useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import logo from "../assets/logo-transparent-png.png";
import "../App.css";
import useToken from "@galvanize-inc/jwtdown-for-react";

function NavBar() {
  const [userId, setUserId] = useState(null);
  const [logoutSuccess, setLogoutSuccess] = useState(false);
  const { logout, token } = useToken();
  const location = useLocation();

  const fetchAccount = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/token`, {
        credentials: "include",
      });
      if (response.ok) {
        const data = await response.json();
        const userId = data.account;
        setUserId(userId);
      }
    } catch (error) {
      console.error("Error fetching account:", error);
    }
  };

  useEffect(() => {
    fetchAccount();
  }, [userId]);

  const handleLogout = async () => {
    try {
      if (token) {
        await logout();
        setLogoutSuccess(true);
        setUserId(null);
        setTimeout(() => {
          setLogoutSuccess(false);
          window.location.href = "/";
        }, 1);
      }
    } catch (error) {
      console.error("Error logging out:", error);
      window.location.href = "/";
    }
  };

  return (
    <nav className="px-3 navbar navbar-expand-lg navbar-light bg-dark site-header sticky-top ">
      <NavLink className="navbar-brand navbar-dark" to="/">
        FitnessQuest
      </NavLink>
      <button
        className="navbar-toggler navbar-dark"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon navbar-dark"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li>
            <NavLink className="nav-link navbar-dark" to="/">
              Home
            </NavLink>
          </li>
          {userId > 0 && (
            <li>
              <NavLink className="nav-link navbar-dark" to="/exercises">
                Exercises
              </NavLink>
            </li>
          )}
          {userId > 0 && (
            <li>
              <NavLink className="nav-link navbar-dark" to="/comments">
                Comments
              </NavLink>
            </li>
          )}
          {userId > 0 && (
            <li>
              <NavLink
                className="nav-link navbar-dark"
                to={`/accounts/${userId}`}
              >
                Edit Account
              </NavLink>
            </li>
          )}
          {userId === null && (
            <li>
              {" "}
              <NavLink className="nav-link navbar-dark" to="/accounts/signup">
                Sign Up
              </NavLink>
            </li>
          )}
          {userId === null && (
            <li>
              <NavLink className="nav-link navbar-dark" to="/accounts/login">
                Login
              </NavLink>
            </li>
          )}
          {userId > 0 && (
            <li>
              <NavLink
                className="nav-link navbar-dark"
                to="/"
                onClick={handleLogout}
              >
                Logout
              </NavLink>
            </li>
          )}
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
