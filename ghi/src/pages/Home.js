import "../App.css";
import ExerciseList from "./ExercisesList";
import ListComments from "./ListComments";
import React, { useEffect, useState } from "react";

export default function Home() {
  const [userId, setUserId] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const fetchAccount = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/token`, {
        credentials: "include",
      });
      if (response.ok) {
        const data = await response.json();
        const userId = data.account;
        setUserId(userId);
      }
    } catch (error) {
      console.error("Error fetching account:", error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchAccount();
    const userIsAuthenticated = true;
    setIsAuthenticated(userIsAuthenticated);
  }, [userId]);

  return (
    <div className="content-container pb-4 bg-light">
      {userId !== null && (
        <>
          <ExerciseList />
          <ListComments />
        </>
      )}
      {userId === null && (
        <>
          <div className="hero-image">
            <div className="hero-text">
              <h1>Track your exercises</h1>
              <p>We got you covered</p>
              <a href="/accounts/signup">
                <button className="hero-button mt-1 mb-3">Sign Up</button>
              </a>
              <p>
                Already a member?{" "}
                <a className="login-link" href="/accounts/login">
                  Log In
                </a>{" "}
              </p>
            </div>
          </div>
          <div className="content-container container marketing">
            <hr className="featurette-divider"></hr>
            <div className="row featurette">
              <div className="col-md-7 order-md-2 d-flex align-items-center mx-auto">
                <div className="d-flex flex-column align-items-center mx-auto">
                  <h2 className="featurette-heading">
                    Fitness Quest is free, easy, convenient.{" "}
                  </h2>
                  <p className="lead">
                    Plan your exercises, track your goals, feel your best
                  </p>
                </div>
              </div>
              <div className="col-md-5 order-md-1">
                <img
                  className="featurette-image img-fluid mx-auto"
                  alt="500x500"
                  style={{ height: "300px" }}
                  src="https://images.pexels.com/photos/4162581/pexels-photo-4162581.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
                  data-holder-rendered="true"
                ></img>
              </div>
            </div>
            <hr className="featurette-divider"></hr>
            <div className="row featurette">
              <div className="col-md-7 d-flex align-items-center mx-auto">
                <div className="d-flex flex-column align-items-center mx-auto">
                  <h2 className="featurette-heading">Who Are We?</h2>
                  <p className="lead">
                    We're a team of health and fitness enthusiasts on a mission
                    to revolutionize people's lives. While taking care of your
                    health is a significant endeavor, we believe it shouldn't
                    always be a challenge. We're firm believers in simplicity
                    and empowerment throughout the process can heighten your
                    chances of successfully reaching your aspirations.
                  </p>
                </div>
              </div>
              <div className="col-md-5">
                <img
                  className="featurette-image img-fluid mx-auto"
                  alt="500x500"
                  style={{ height: "300px" }}
                  src="https://images.pexels.com/photos/1954524/pexels-photo-1954524.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
                  data-holder-rendered="true"
                ></img>
              </div>
            </div>
            <hr className="featurette-divider"></hr>
            <div className="row featurette">
              <div className="col-md-7 order-md-2 d-flex align-items-center mx-auto">
                <div className="d-flex flex-column align-items-center mx-auto">
                  <h2 className="featurette-heading">Customer Reviews </h2>
                  <p className="lead">
                    "This app redefined how I approach my fitness goals,
                    offering an intuitive platform to track my exercises and
                    progress with ease. Its straightforward interface keeps me
                    motivated, and the convenience of having all my fitness data
                    in one place is truly remarkable."
                  </p>
                </div>
              </div>
              <div className="col-md-5 order-md-1">
                <img
                  className="featurette-image img-fluid mx-auto"
                  alt="500x500"
                  style={{ height: "300px" }}
                  src="https://images.pexels.com/photos/1654489/pexels-photo-1654489.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
                  data-holder-rendered="true"
                ></img>
              </div>
            </div>
            <hr className="featurette-divider"></hr>
          </div>
        </>
      )}
    </div>
  );
}
