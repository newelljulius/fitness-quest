# Fitness Quest

## The Team

- Dakota Steppe
- Yohan Pak
- Kathy Gomez
- Julius Pagtakhan

## Database Diagram

![Alt text](dbdiagram.png)

## Functionality

- Upon landing on the home page, Users who do not have an account registered will see information about Fitness Quest, including features, about us, and a testimonial.
- Users will be required to Sign Up or Log In to access the app.
- Once logged in, Users can create, view, update, or delete their exercises.
- Users can create, view, update, or delete comment notes tied to each of their listed exercises.

## Project Initialization

- To fully enjoy this application on your local machine, please make sure to follow these steps:
- Clone the repository down to your local machine
- CD into the new project directory
- Run `docker volume create fastapi-fitness-data``
- Run `docker compose build``
- Run `docker compose up``
- Exit the container's CLI, and enjoy planning and tracking your exercises!

## API Documentation

- For full documentation, visit http://localhost:8000/docs once project is initialized (see above)
